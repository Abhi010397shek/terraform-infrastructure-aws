#Elastic Cache Subnet Group
resource "aws_elasticache_subnet_group" "elasticsubnet" {
  name       = "elasticsubnet"
  subnet_ids = var.subnet_ids1
}

#AWS Elastic Cache
resource "aws_elasticache_cluster" "cached" {
  cluster_id           = var.cluster_id
  engine               = var.engine
  node_type            = var.node_type
  engine_version       = var.engine_version
  availability_zone    = var.cluster_availability_zone
  maintenance_window   = var.cluster_maintenance_window
  num_cache_nodes      = var.num_cache_nodes
  subnet_group_name    = aws_elasticache_subnet_group.elasticsubnet.name
  parameter_group_name = var.parameter_group_name
  tags = {
    Name = "Elasticache"
  }
}
