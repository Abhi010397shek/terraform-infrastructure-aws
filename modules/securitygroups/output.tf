
output "aws_db_security_group_id" {
  value = aws_security_group.sgprivatedb.id
}

output "aws_lb_security_group_id" {
  value = aws_security_group.securityloadbalancer.id
}

output "nat_id" {
  value = aws_security_group.nat.id
}
